import java.util.Arrays;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.ForkJoinPool;

public class HalfParallelMergesort extends RecursiveAction{

    private static int[] array;
    private int lo = 0;
    private int hi;
    private int[] temp;

    public HalfParallelMergesort(int[] array){
        HalfParallelMergesort.array = array;
        this.hi = array.length-1;
        this.temp = new int[array.length];
    }


    private HalfParallelMergesort(int lo, int hi){
        this.lo=lo;
        this.hi=hi;
    }

    @Override
    protected void compute(){
        if(lo < hi){
            int m = lo + (hi - lo) / 2;
            HalfParallelMergesort left = new HalfParallelMergesort(lo, m);
            HalfParallelMergesort right = new HalfParallelMergesort(m+1, hi);
            left.fork();
            right.compute();
            left.join();
            merge(lo, m, hi);
        }
    }

    public void merge(int lo, int m, int hi){
        temp = Arrays.copyOf(array, array.length);
        int i = lo;
        int j = m+1;
        int k = lo;
        while(i <= m && j <= hi){
            if(temp[i] <= temp[j]){
                array[k] = temp[i];
                i++;
            }
            else{
                array[k] = temp[j];
                j++;
            }
            k++;
        }
        while(i <= m){
            array[k] = temp[i];
            k++;
            i++;
        }
    }

}
