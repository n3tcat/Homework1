import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;
public class Main{

    public static int[] generateRandom(int size){
        int[] arr = new int[size];
        for(int i=0; i<arr.length; i++){
            arr[i] = (int)(Math.random()*10);
        }
        return arr;
    }

    public static int[] generateDescend(int size){
        int[] arr = new int[size];
        for(int i = size-1; i>-1; i--){
            arr[arr.length-(i+1)] = i;
        }
        return arr;
    }

    public static void check(int[] array){
        for(int i = 1; i<array.length; i++){
            if(array[i-1]>array[i]){System.out.println("Wrong execution."); System.exit(1);}
        }
        System.out.println("\nOrdered!");
    }

    public static int[] sel(int sel, int n){
        int[] array = new int[1];
        switch(sel){
            case 1: array = generateDescend(n);break;
            case 2: array = generateRandom(n);break;
        }
        return array;
    }

    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);
        System.out.println("Selezionare la modalita' di test inserendo il numero corrispondente:\n1: array discendente\n2: array casuale");
        int sel = scan.nextInt();
        while(sel < 1 || sel > 2){
            System.out.println("opzione non valida, riprova:");
            sel = scan.nextInt();
        }
        System.out.println("\nInserire la dimensione n dell'array:");
        int n = scan.nextInt();
        int[] array = sel(sel, n);
        
        System.out.println("\n################################################################################################");
        System.out.println("\nTest mergesort sequenziale:");
        System.out.println("\n################################################################################################");
        System.out.print("\narray iniziale: ");
        System.out.println(Arrays.toString(array));
        double start = (double) System.currentTimeMillis();
        SeqMergesort seq1 = new SeqMergesort(array);
        double end = (double) System.currentTimeMillis();
        check(array);
        System.out.print("\narray ordinato :");
        System.out.println(Arrays.toString(array));
        System.out.println("\nexecution time: "+(end-start)/1000+"s");

        System.out.println("\n################################################################################################");
        System.out.println("\nTest mergesort parallelo basato su merge sequenziale:");
        System.out.println("\n################################################################################################");
        array = sel(sel, n);
        System.out.print("\narray iniziale: ");
        System.out.println(Arrays.toString(array));
        ForkJoinPool fjp = new ForkJoinPool();
        start = (double) System.currentTimeMillis();
        HalfParallelMergesort halfSeq1 = new HalfParallelMergesort(array);
        fjp.invoke(halfSeq1);
        end = (double) System.currentTimeMillis();
        check(array);
        System.out.print("\narray ordinato: ");
        System.out.println(Arrays.toString(array));
        System.out.println("\nexecution time: "+(end-start)/1000+"s");
        fjp.shutdown();

        System.out.println("\n################################################################################################");
        System.out.println("\nTest mergesort parallelo basato su merge parallelo:");
        System.out.println("\n################################################################################################");
        array = sel(sel, n);
        System.out.print("\narray iniziale: ");
        System.out.println(Arrays.toString(array));
        ForkJoinPool fjp1 = new ForkJoinPool();
        start = (double) System.currentTimeMillis();
        FullParallelMergesort fullPar = new FullParallelMergesort(array);
        fjp1.invoke(fullPar);
        end = (double) System.currentTimeMillis();
        // check(array);
        System.out.print("\narray ordinato: ");
        System.out.println(Arrays.toString(array));
        System.out.println("\nexecution time: "+(end-start)/1000+"s");
        fjp1.shutdown();
    }
}